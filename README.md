<h1>JSON Web Token Decoder - JWTDecoder</h1>

<h2>Introduction</h2>
<p>This is a JavaFX desktop application.  The application allows a user to enter a JSON Web Token and ecode the JWT to display the Header and Payload parts of the token. </p> 

<img src="https://softwarepulse.co.uk/wp-content/uploads/2021/05/JWT-screen.jpg" width="600" height="375">

<p>The application was built with a four person team with the aim to learn and improve the teams application developement skills.  As this was the first project for the team, the application was a short and simple project to get everyone use to working together.</p>

<h2>Team members:</h2>
<ul>
<li>Klaus Capani, </li>
<li>Josh James, </li>
<li>John McNeil, </li>
<li>Jorid Spaha</li>
</ul>

<h2>Technologies</h2>
<p>The application is written using OpenJDK 11 with OpenJFX 11.  It is distributed as either a Windows MSI installer or Linux Debian installer.  Both the windows and Linux installers are created using the OpenJDK 11 JPackager.exe tool.  </p>

<ul> 
<li><a href="https://softwarepulse.co.uk/blog/javafx-11-desktop-msi-application/">Example of building JavaFX Windows MSI with Wix Toolset</a></li>
<li><a href="https://softwarepulse.co.uk/blog/linux_javafx_desktop_application/">Example of building Linux Debian installer</a></li>
</ul>

<h2>Launch</h2>
To see the application in action watch the short YouTube video.
<ul> 
<li><a href="https://youtu.be/-L_yNQIpl3k">YouTube vidoe JWTDecoder Java desktop application</a></li>
<li><a href="https://gitlab.com/johnmc-sp/json-web-token-decoder/-/raw/main/Installers/JWTDecoder-1.0.msi">The Windows MSI installer can be dowloaded from here</a></li>
<li><a href="https://gitlab.com/johnmc-sp/json-web-token-decoder/-/raw/main/Installers/JWTDecoder-1.1.deb">The Linux Debian installer can br downloaded from here</a></li>
</ul>
